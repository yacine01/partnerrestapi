package com.b2boost.itw.controller;

import com.b2boost.itw.dto.PartnerRequestDTO;
import com.b2boost.itw.entity.Partner;
import com.b2boost.itw.service.PartnerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class PartnerControllerTest {
    @Autowired
    private MockMvc mockMvc;
    private ObjectMapper objectMapper= new ObjectMapper();

    @MockBean
    private PartnerService partnerService;

    private Long idPartner = 1L;



    /**
     * Test case for handling bad request when creating a partner.
     */
    @Test
    void testCreatePartner_BadRequest() throws Exception {
        // Perform POST request with missing required fields
        mockMvc.perform(post("/api/partners"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test case for successful creation of a partner.
     */
    @Test
    void testCreatePartner_Success() throws Exception {
        // Mock data
        PartnerRequestDTO partnerRequestDTO = new PartnerRequestDTO("name", "XXXXX", "en_ES", "2017-10-03T12:18:46+00:00");

        Partner createdPartner = Partner.builder().id(idPartner).name("name").reference("XXXXX").locale("en_ES").expirationTime("2017-10-03T12:18:46+00:00").build();
        when(partnerService.createPartner(any(PartnerRequestDTO.class))).thenReturn(createdPartner);

        // Perform POST request
        mockMvc.perform(post("/api/partners").contentType(MediaType.APPLICATION_JSON).content(objectMapper.writeValueAsString(partnerRequestDTO))).andExpect(status().isCreated());
    }

    /**
     * Test case for successful retrieval of all partners.
     */
    @Test
    void testGetAllPartners_Success() throws Exception {
        // Mock data
        Page<Partner> page = new PageImpl<>(List.of(new Partner(), new Partner()));
        when(partnerService.getAllPartners(any(Pageable.class))).thenReturn(page);

        // Perform GET request
        mockMvc.perform(get("/api/partners"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    /**
     * Test case for successful retrieval of a partner by ID.
     */
    @Test
    void testGetPartnerById_Success() throws Exception {
        Partner createdPartner = Partner.builder().id(idPartner).name("name").reference("XXXXX").locale("en_ES").expirationTime("2017-10-03T12:18:46+00:00").build();
        when(partnerService.getPartnerById(anyLong())).thenReturn(Optional.of(createdPartner));

        // Perform GET request and verify response
        mockMvc.perform(get("/api/partners/"+idPartner))
                .andExpect(status().isOk());

    }


    /**
     * Test case for handling not found when retrieving a partner by ID.
     */
    @Test
    void testGetPartnerById_NotFound() throws Exception {
        // Mock data
        when(partnerService.getPartnerById(idPartner)).thenReturn(Optional.empty());

        // Perform GET request
        mockMvc.perform(get("/api/partners/1"))
                .andExpect(status().isNotFound());
    }

    /**
     * Test case for handling bad request when updating a partner.
     */
    @Test
    void testUpdatePartner_BadRequest() throws Exception {
        // Perform PUT request with missing required fields
        mockMvc.perform(put("/api/partners/1"))
                .andExpect(status().isBadRequest());
    }

    /**
     * Test case for successful update of a partner.
     */
    @Test
    void testUpdatePartner_Success() throws Exception {
        // Mock data
        PartnerRequestDTO updatedPartnerRequestDTO = new PartnerRequestDTO("name", "XXXXX", "en_ES", "2017-10-03T12:18:46+00:00");

        Partner updatedPartner = new Partner();
        updatedPartner.setId(idPartner);
        when(partnerService.updatePartner(idPartner, updatedPartnerRequestDTO)).thenReturn(Optional.of(updatedPartner));

        // Perform PUT request
        mockMvc.perform(put("/api/partners/"+idPartner)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(updatedPartnerRequestDTO)))
                .andExpect(status().isOk());

    }

    /**
     * Test case for successful deletion of a partner.
     */
    @Test
    void testDeletePartner_Success() throws Exception {
        // Mock data
        Partner createdPartner = Partner.builder().id(idPartner).name("name").reference("XXXXX").locale("en_ES").expirationTime("2017-10-03T12:18:46+00:00").build();
        when(partnerService.getPartnerById(idPartner)).thenReturn(Optional.of(createdPartner));

        // Perform DELETE request and verify response
        mockMvc.perform(delete("/api/partners/"+idPartner))
                .andExpect(status().isOk());
    }



}