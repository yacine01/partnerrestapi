package com.b2boost.itw.service;

import com.b2boost.itw.dto.PartnerRequestDTO;
import com.b2boost.itw.entity.Partner;
import com.b2boost.itw.repository.PartnerRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PartnerServiceImplTest {

    @Mock
    PartnerRepository partnerRepository;

    @InjectMocks
    PartnerServiceImpl partnerService;

    Long partnerId = 1L;

    Partner partner ;

    PartnerRequestDTO partnerRequestDTO;

    @BeforeEach
    void before(){

        partnerRequestDTO = new PartnerRequestDTO("name", "XXXXX", "en_ES", "2017-10-03T12:18:46+00:00");

        partner = Partner.builder().id(partnerId).name("name").reference("XXXXX").locale("en_ES").expirationTime("2017-10-03T12:18:46+00:00").build();
    }

    @Test
    void getAllPartners() {
        int from = 0;
        int size = 10;

        List<Partner> partners = new ArrayList<>();
        partners.add(Partner.builder().id(1L).name("name").reference("XXXXX").locale("en_ES").expirationTime("2017-10-03T12:18:46+00:00").build());
        partners.add(Partner.builder().id(2L).name("name2").reference("1234").locale("en_ES").expirationTime("2017-10-03T12:18:46+00:00").build());

        Pageable pageable = PageRequest.of(from, size);
        Page<Partner> pagedResult = new PageImpl<>(partners, pageable, partners.size());

        when(partnerRepository.findAll(any(Pageable.class))).thenReturn(pagedResult);

        Page<Partner> partnersResult = partnerService.getAllPartners(pageable);
        assertNotNull(partnersResult);
        assertEquals(partnersResult.getTotalElements(),pagedResult.getTotalElements());
    }

    @Test
    void getPartnerById__should_return_the_partner_if_exist() {
        when(partnerRepository.findById(partnerId)).thenReturn(
                Optional.of(partner)
        );

        Optional<Partner> retrievedPartner = partnerService.getPartnerById(partnerId);
        retrievedPartner.ifPresent(value -> assertEquals(value, retrievedPartner.get()));
        verify(partnerRepository).findById(partnerId);
    }

    @Test
    void createPartner__should_return_the_created_partner() {
        when(partnerRepository.save(any())).thenReturn(partner);

        Partner savedPartnerResponse = partnerService.createPartner(partnerRequestDTO);

        assertNotNull(savedPartnerResponse);
        assertEquals(partner.getName(), savedPartnerResponse.getName());
        assertEquals(partner.getReference(), savedPartnerResponse.getReference());
        assertEquals(partner.getLocale(), savedPartnerResponse.getLocale());
        assertEquals(partner.getExpirationTime(), savedPartnerResponse.getExpirationTime());

        verify(partnerRepository).save(any());
    }

    @Test
    void updatePartner() {
        when(partnerRepository.findById(partnerId)).thenReturn(Optional.of(partner));
        Partner partnerUpdated = Partner.builder()
                .id(partnerId)
                .name(partner.getName())
                .reference(partner.getReference())
                .locale(partner.getLocale())
                .expirationTime(partner.getExpirationTime())
                .build();
        when(partnerRepository.save(any())).thenReturn(partnerUpdated);
        Optional<Partner> partnerUpdatedFromService= partnerService.updatePartner(partnerId,partnerRequestDTO);
        assertTrue(partnerUpdatedFromService.isPresent());
    }

    @Test
    void deletePartner() {
        doNothing().when(partnerRepository).deleteById(partnerId);

        partnerService.deletePartner(partnerId);

        verify(partnerRepository, times(1)).deleteById(partnerId);

    }


    @Test
    public void testFindByRef_ExistingPartner() {

        when(partnerRepository.findByReference(partner.getReference())).thenReturn(Optional.of(partner));

        boolean result = partnerService.findByRef(partner.getReference());

        verify(partnerRepository, times(1)).findByReference(partner.getReference());

        assertTrue(result);
    }

    @Test
    public void testFindByRef_NonExistingPartner() {

        when(partnerRepository.findByReference(partner.getReference())).thenReturn(Optional.empty());

        boolean result = partnerService.findByRef(partner.getReference());

        verify(partnerRepository, times(1)).findByReference(partner.getReference());

        assertFalse(result);
    }
}