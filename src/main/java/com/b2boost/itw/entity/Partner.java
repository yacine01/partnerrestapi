package com.b2boost.itw.entity;

import jakarta.persistence.*;
import lombok.*;

@Entity
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "partner")
public class Partner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NonNull
    @Column(name = "company_name")
    private String name;

    @NonNull
    @Column(name = "ref", unique = true)
    private String reference;
    @NonNull
    @Column(name = "locale")
    private String locale;
    @NonNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expires")
    private String expirationTime;

}
