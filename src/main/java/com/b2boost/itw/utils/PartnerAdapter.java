package com.b2boost.itw.utils;


import com.b2boost.itw.dto.PartnerRequestDTO;
import com.b2boost.itw.entity.Partner;

public class PartnerAdapter {
    public static Partner fromDTO(PartnerRequestDTO partnerRequestDTO) {
        return Partner.builder()
                .name(partnerRequestDTO.name())
                .reference(partnerRequestDTO.reference())
                .locale(partnerRequestDTO.locale())
                .expirationTime(partnerRequestDTO.expirationTime())
                .build();
    }

}
