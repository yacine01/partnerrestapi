package com.b2boost.itw.dto;

public record PartnerRequestDTO(String name, String reference, String locale, String expirationTime ) {
}
