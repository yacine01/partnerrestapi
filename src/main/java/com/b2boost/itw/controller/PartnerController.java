package com.b2boost.itw.controller;

import com.b2boost.itw.dto.PartnerRequestDTO;
import com.b2boost.itw.entity.Partner;
import com.b2boost.itw.service.PartnerService;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
/**
 * Controller class for handling Partner-related HTTP requests.
 */
@RestController
@AllArgsConstructor
@RequestMapping("/api/partners")
public class PartnerController {

    private PartnerService partnerService;

    /**
     * Retrieves a paginated list of partners.
     *
     * @param from Starting index of the result set to paginate to (default: 0).
     * @param size Window pagination size (default: 10).
     * @return ResponseEntity containing a paginated list of partners or an error message if an internal error occurs.
     */
    @GetMapping
    public ResponseEntity<?> getAllPartners(@RequestParam(defaultValue = "0") int from,
                                                        @RequestParam(defaultValue = "10") int size) {
        try {
            // Retrieve partners from the service
            Page<Partner> partnersPage = partnerService.getAllPartners(PageRequest.of(from, size));
            List<Partner> partners = partnersPage.getContent();
            return new ResponseEntity<>(partners, HttpStatus.OK);
        } catch (Exception e) {
            // Handling internal errors
            return new ResponseEntity<>("An internal error occurred : " +e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Retrieves a partner by its ID.
     *
     * @param id The ID of the partner to retrieve.
     * @return ResponseEntity containing the partner with the specified ID or a message indicating the partner was not found.
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getPartnerById(@PathVariable("id") Long id) {
        Optional<Partner> partner = partnerService.getPartnerById(id);
        if (partner.isPresent()) {
            return new ResponseEntity<>(partner.get(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Partner with id "+id+" not found",HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Creates a new partner.
     *
     * @param partnerRequest The request body containing information about the new partner.
     * @return ResponseEntity containing the created partner or an error message if validation fails or an internal error occurs.
     */
    @PostMapping
    public ResponseEntity<?> createPartner(@RequestBody PartnerRequestDTO partnerRequest) {
        try {
            // Validating partner data
            if (partnerRequest.name().isBlank() || partnerRequest.reference().isBlank() || partnerRequest.locale().isBlank() || partnerRequest.expirationTime().isBlank()) {
                return new ResponseEntity<>("Missing required fields", HttpStatus.BAD_REQUEST);
            }

            // Check if the partner already exists
            if (partnerService.findByRef(partnerRequest.reference())) {
                return new ResponseEntity<>("Partner already exists", HttpStatus.BAD_REQUEST);
            }

            // Create Partner

            Partner createdPartner = partnerService.createPartner(partnerRequest);
            return new ResponseEntity<>(createdPartner, HttpStatus.CREATED);
        } catch (Exception e) {
            // Handling internal errors
            return new ResponseEntity<>("An internal error occurred : " +e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    /**
     * Updates an existing partner.
     *
     * @param id               The ID of the partner to update.
     * @param updatedPartnerDTO The request body containing updated information for the partner.
     * @return ResponseEntity containing the updated partner or an error message if validation fails, the partner is not found, or an internal error occurs.
     */
    @PutMapping("/{id}")
    public ResponseEntity<?> updatePartner(@PathVariable("id") Long id, @RequestBody PartnerRequestDTO updatedPartnerDTO) {

        try{
            if (updatedPartnerDTO.name().isBlank() || updatedPartnerDTO.reference().isBlank() || updatedPartnerDTO.locale().isBlank() || updatedPartnerDTO.expirationTime().isBlank()) {
                return new ResponseEntity<>("Missing required fields", HttpStatus.BAD_REQUEST);
            }
           Optional<Partner> updatedPartnerOptional = partnerService.updatePartner(id, updatedPartnerDTO);
           if (updatedPartnerOptional.isPresent()) {
               return new ResponseEntity<>(updatedPartnerOptional.get(), HttpStatus.OK);
           } else {
               return new ResponseEntity<>("Partner with id " + id + " not found.", HttpStatus.NOT_FOUND);
           }
       }catch (Exception e) {
           // Handling internal errors
           return new ResponseEntity<>("An internal error occurred : " +e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
       }

    }
    /**
     * Deletes a partner by its ID.
     *
     * @param id The ID of the partner to delete.
     * @return ResponseEntity indicating success or failure of the deletion operation.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deletePartner(@PathVariable("id") Long id) {

        try {
            // check partner is existing
            if (partnerService.getPartnerById(id).isEmpty()) {
                return new ResponseEntity<>("Partner with id " + id + " not found.", HttpStatus.NOT_FOUND);
            }

            // delete partner
            partnerService.deletePartner(id);
            return new ResponseEntity<>("The partner with the given id has been deleted", HttpStatus.OK);
        } catch (Exception e) {
            // Handling internal errors
            return new ResponseEntity<>("An internal error occurred : "+e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
