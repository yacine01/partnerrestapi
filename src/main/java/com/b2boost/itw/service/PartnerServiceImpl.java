package com.b2boost.itw.service;

import com.b2boost.itw.dto.PartnerRequestDTO;
import com.b2boost.itw.entity.Partner;
import com.b2boost.itw.repository.PartnerRepository;
import com.b2boost.itw.utils.PartnerAdapter;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Implementation of the PartnerService interface providing CRUD operations for Partner entities.
 */
@AllArgsConstructor
@Service
public class PartnerServiceImpl implements PartnerService {
    /**
     * Repository for accessing Partner entities in the database.
     */
    private PartnerRepository partnerRepository;

    /**
     * Retrieves a paginated list of partners.
     *
     * @param pageable Pagination information.
     * @return Page containing a list of partners.
     */
    @Override
    public Page<Partner> getAllPartners(Pageable pageable) {
        return partnerRepository.findAll(pageable);
    }

    /**
     * Retrieves a partner by its ID.
     *
     * @param id The ID of the partner to retrieve.
     * @return An Optional containing the partner if found, otherwise an empty Optional.
     */
    @Override
    public Optional<Partner> getPartnerById(Long id) {
        return partnerRepository.findById(id);
    }

    /**
     * Creates a new partner.
     *
     * @param partner The PartnerRequestDTO containing information about the new partner.
     * @return The created partner.
     */
    @Override
    public Partner createPartner(PartnerRequestDTO partner) {
        return partnerRepository.save(PartnerAdapter.fromDTO(partner)
        );
    }

    /**
     * Updates an existing partner.
     *
     * @param id               The ID of the partner to update.
     * @param updatedPartner   The PartnerRequestDTO containing updated information for the partner.
     * @return An Optional containing the updated partner if found and successfully updated, otherwise an empty Optional.
     */
    @Override
    public Optional<Partner> updatePartner(Long id, PartnerRequestDTO updatedPartner) {
        Optional<Partner> existingPartnerOptional = partnerRepository.findById(id);
        if (existingPartnerOptional.isPresent()) {
            Partner updated = PartnerAdapter.fromDTO(updatedPartner);
            updated.setId(id);
            Partner savedPartner = partnerRepository.save(updated);
            return Optional.of(savedPartner);
        } else {
            return Optional.empty();
        }
    }
    /**
     * Deletes a partner by its ID.
     *
     * @param id The ID of the partner to delete.
     */
    @Override
    public void deletePartner(Long id) {
        partnerRepository.deleteById(id);
    }

    /**
     * Checks if a partner with the given reference exists.
     *
     * @param reference The reference of the partner to search for.
     * @return True if a partner with the given reference exists, otherwise false.
     */
    @Override
    public boolean findByRef(String reference) {
        Optional<Partner> existingPartner = partnerRepository.findByReference(reference);
        return existingPartner.isPresent();
    }
}
