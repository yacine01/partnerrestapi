package com.b2boost.itw.service;

import com.b2boost.itw.dto.PartnerRequestDTO;
import com.b2boost.itw.entity.Partner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

public interface PartnerService {

        Page<Partner> getAllPartners(Pageable pageable);

        Optional<Partner> getPartnerById(Long id);

        Partner createPartner(PartnerRequestDTO partner);

        Optional<Partner> updatePartner(Long id, PartnerRequestDTO updatedPartner);

        void deletePartner(Long id);

        boolean findByRef(String reference);

}
