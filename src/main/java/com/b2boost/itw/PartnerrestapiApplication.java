package com.b2boost.itw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PartnerrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(PartnerrestapiApplication.class, args);
	}

}
